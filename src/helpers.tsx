import _ from 'lodash';

/**
// partitionItems() takes an array of Item objects and groups them by type. 
// There is a key for each type set to an array of Item objects gruouped by type 
//
// An additional selected key with boolean value is added to each Item object 
// This selected property will be used to record the state of the stepper
//
// An additional None object will be added to each grouping array 
// to allow a user to pass on a selection (it is selected by default)
//
//
// partitionedItems() 
//   input example :
//     [
//       {
//         type: 'WATER_FEATURES', 
//         lowPrice: 2000000, 
//         name: 'Fountain  
//         highPrice: 6000000
//       },
//       {...},
//       {...},
//     ]
//  
//  
//   output example =>
//     {
//       DECK_MATERIAL: [
//         {
//           highPrice: 1400000
//           lowPrice: 1200000
//           name: "Redwood"
//           type: "DECK_MATERIAL"
//           selected: false
//         }, 
//         {
//           type: '',
//           name: 'none',
//           lowPrice: 0,
//           highPrice: 0,
//           selected: true,
//         },
//         {...}
//         {...}
//       ],
//       FENCING_AND_PRIVACY: [
//         {...},
//         {...}
//       ],
//     }
//
//
//
// formatNumber(number) returns a string of a number with commas added
//   input  60000
//   output 60,000
// 
**/

interface Item {
  type: string;
  name: string;
  lowPrice: number;
  highPrice: number;
  selected?: boolean;
}

interface PartitionedItems {
  [key: string]: Item[]
}

function partitionItems(items: Item[]) {
  let partitionedItems: PartitionedItems = {}

  for (let item of items) {
    item = {...item, selected: false}

    if (item.type in partitionedItems) {
      partitionedItems[item.type].push(item);
    } else {
      partitionedItems[item.type] = [item];
    }
  }

  partitionedItems = removeDuplicates(partitionedItems);
  partitionedItems = removeDecimals(partitionedItems);
  partitionedItems = addNoneObjects(partitionedItems);
  
  return partitionedItems;
}


function removeDuplicates(partitionedItems: PartitionedItems): PartitionedItems {
  // Removes all Item objects that have the same name among grouped keys
  
  for (let group in partitionedItems) {
    partitionedItems[group] = _.uniqBy(partitionedItems[group], 'name');
  }

  return partitionedItems;
}


function removeDecimals(partitionedItems: PartitionedItems): PartitionedItems {
  // Removes all decimals places from all items
  // example input: [{lowPrice: 6000000...}, {...}]
  // example output: [{lowPrice: 60000...}, {...}]

  for (let group in partitionedItems) {
    const itemArr = partitionedItems[group];

    for (let item of itemArr) {
      const formatedLowPrice = String(item.lowPrice).slice(0, -2);
      const formatedHighPrice = String(item.highPrice).slice(0, -2);

      item.lowPrice = Number(formatedLowPrice);
      item.highPrice = Number(formatedHighPrice);
    }
  }

  return partitionedItems;
}


function addNoneObjects(partitionedItems: PartitionedItems): PartitionedItems {
  // Users will be selecting an Item Object from each partitionedItems grouping
  // A noneObject is added to each grouping array and is selected by default

  const noneObject = {
    type: '',
    name: 'none',
    lowPrice: 0,
    highPrice: 0,
    selected: true,
  }

  for (let group in partitionedItems) {
    partitionedItems[group].unshift(noneObject);
  }

  return partitionedItems;
}


function formatNumber(num: string): string {
  // convert number into readable format
  // example input: 60000
  // example output: 60,000

  return Number(num).toLocaleString();
}

export {
  partitionItems,
  formatNumber,
}
