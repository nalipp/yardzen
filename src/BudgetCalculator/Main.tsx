import { useState, useEffect } from 'react';

import { partitionItems } from '../helpers';
import getItems from '../api';

import BudgetIntake from './BudgetIntake';
import BudgetStepper from './BudgetStepper';
import EstimateTracker from './EstimateTracker';

import Box from '@mui/material/Box';


interface Item {
  type: string;
  name: string;
  lowPrice: number;
  highPrice: number;
  selected?: boolean;
}

interface PartitionedItems {
  [key: string]: Item[];
}


function Main() {
  // Parrent element for all budget calculator related components 
  //
  // The budgetSubmited state is set to false until budget is submitted
  //
  // The useEffect api request happens while the user completes
  //   the budget input to reduce waiting
  //
  // activeStep controls the state of the stepper and is held in Main
  //   to preserve user location across budget updates
  // 
  //

  const [ budget, setBudget ] = useState<any>('');
  const [ budgetSubmited, setBudgetSubmited ] = useState<boolean>(false);
  const [ activeStep, setActiveStep ] = useState<number>(0);
  const [ stepNames, setStepNames ] = useState<string[]>([]);
  const [ partitionedItems, setPartitionedItems ] = (
    useState<PartitionedItems>({} as PartitionedItems)
  );

  useEffect(() => {
    // Api call to getItems() returns an array of Item objects
    //
    // The partitionedItems() helper function converts the array of Item
    //   objects to a single object with the Items grouped by type
    //   it also adds a selected key to track if an Item has been selected
    //   and a noneObject allowing users to decline selecting from a group
    //
    async function requestItems() {
      const items = await getItems();
      const partitionedItems = partitionItems(items);

      setPartitionedItems(partitionedItems);
      setStepNames(Object.keys(partitionedItems));
    }

    requestItems();
  }, []);

  return (
    <Box>
      <Box sx={{ marginTop: 3, marginLeft: 3 }}>
        <h2>Yardzen</h2>
        <h4>Budget Calculator</h4>
      </Box>

      <Box>
      {
        !budgetSubmited
        ? 
          <BudgetIntake 
            setBudgetSubmited={setBudgetSubmited}
            setBudget={setBudget}
            budget={budget}
          />
        :
          <Box sx={{ 
            marginTop: 10,
            display: 'flex', 
            justifyContent: 'space-evenly', 
            flexWrap: 'wrap',
            maxWidth: 800}}
          >
            <BudgetStepper 
              setPartitionedItems={setPartitionedItems}
              stepNames={stepNames}
              partitionedItems={partitionedItems}
              setActiveStep={setActiveStep}
              activeStep={activeStep}
            />

            <EstimateTracker 
              setBudgetSubmited={setBudgetSubmited}
              partitionedItems={partitionedItems}
              budget={budget}
            />

          </Box>
      }
      </Box>
    </Box>
  )
}

export default Main;
