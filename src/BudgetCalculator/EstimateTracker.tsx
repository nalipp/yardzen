import { 
  SetStateAction,
  useState,
  useEffect,
  Dispatch,
  Fragment 
} from 'react';

import { 
  Box,
  Button,
  Typography,
  Card,
  CardActions,
  CardContent 
} from '@mui/material';

import { formatNumber } from '../helpers';

interface Item {
  type: string;
  name: string;
  lowPrice: number;
  highPrice: number;
  selected?: boolean;
}

interface PartitionedItems {
  [key: string]: Item[]
}

interface EstimateTrackerProps {
  partitionedItems: PartitionedItems;
  setBudgetSubmited: Dispatch<SetStateAction<boolean>>;
  budget: number;
}

function EstimateTracker({ 
    partitionedItems,
    setBudgetSubmited,
    budget,
  }: EstimateTrackerProps) {

  //
  // EstimateTracker calculates the total of their current Item 
  //   lowPrice and highPrice estimates. It gives color feedback 
  //   if the user is inside or outside their budget range
  //
  //   EstimateTracker recalculates the minEstimate and maxEstimate
  //     whenever the state of an Item in partitionedItems has
  //     it's selected state changed. 
  //     (whenever an item is selcted in the BudgetStepper)
  //

  const [minEstimate, setMinEstimate] = useState<number>(0);
  const [maxEstimate, setMaxEstimate] = useState<number>(0);

  useEffect(() => {
    calculateCurTotal()
  }, [minEstimate, maxEstimate, partitionedItems]);

  function calculateCurTotal(): void {
    //
    // Set minEstimate and maxEstimate based on current total
    
    let minValTotal = 0;
    let maxValTotal = 0;

    for (let groupKey in partitionedItems) {
      const itemsArr = partitionedItems[groupKey];
      
      for (let item of itemsArr) {
        if (item.selected === true) {
          minValTotal += item.lowPrice;
          maxValTotal += item.highPrice;
        }
      }
    }
    setMinEstimate(minValTotal);
    setMaxEstimate(maxValTotal);
  }

  function handleClick() {
    setBudgetSubmited(false);
  }

  function getCurTotalRange() {
    return `
      ${formatNumber(String(minEstimate))} - 
      ${formatNumber(String(maxEstimate))}`;
  }

  function getRangeColor() {
    return minEstimate < budget ? 'green' : 'tomato';
  }

  const card = (
    <Fragment>
      <CardContent>
        <Typography variant="h5" component="div">
          Estimate Tracker
        </Typography>

        <Typography 
          sx={{ 
            fontSize: 14, 
            color: getRangeColor()
          }} 
          color="text.secondary" 
          gutterBottom
        >
          Budget {formatNumber(String(budget))}
        </Typography>
        
        <Typography 
          sx={{ fontSize: 14 }} 
          color="text.secondary"
          gutterBottom
        >
          Current total range : {getCurTotalRange()}
        </Typography>
      </CardContent>
      <CardActions>
        <Button 
          onClick={handleClick} 
          size="small"
        >
          Edit Budget
        </Button>
      </CardActions>
    </Fragment>
  );

  return (
    <Box sx={{ minWidth: 300, marginBottom: 10}}>
      <Box sx={{ mx: 'auto' }}>
        <Card variant="outlined">{card}</Card>
      </Box>
    </Box>
  );
}

export default EstimateTracker;
