import { 
  ChangeEvent,
  SetStateAction,
  Dispatch,
  Fragment 
} from 'react';

import {
  Box,
  Stepper,
  Step,
  StepLabel,
  StepContent,
  Button,
  Paper,
  Typography,
} from '@mui/material';

import { FormControl } from '@material-ui/core';

import { formatNumber } from '../helpers';


interface Item {
  type: string;
  name: string;
  lowPrice: number;
  highPrice: number;
  selected?: boolean;
}

interface PartitionedItems {
  [key: string]: Item[]
}

interface BudgetStepperProps {
  setPartitionedItems: Dispatch<SetStateAction<PartitionedItems>>;
  stepNames: string[];
  partitionedItems: PartitionedItems;
  setActiveStep: Dispatch<SetStateAction<number>>;
  activeStep: number;
}


function BudgetStepper({ 
    setPartitionedItems,
    stepNames,
    partitionedItems,
    setActiveStep,
    activeStep
  }: BudgetStepperProps) {

// BudgetStepper controls the flow of inputs for selecting 
//   one Item object for eath Item grouping, the main styling
//   for this component comes directly from the Material UI docs
//
// The paritionedItems object had a key name pointing to an 
//   array of all Item objects that belong to that groping
//
//   {
//     DECK_MATERIAL: [
//       {
//         highPrice: 1400000
//         lowPrice: 1200000
//         name: "Redwood"
//         type: "DECK_MATERIAL"
//         selected: false;
//       }, 
//       {...}
//     ],
//     FENCING_AND_PRIVACY: [
//       {...},
//       {...}
//     ],
//   }
// ];


  function handleChange(evt: ChangeEvent<HTMLInputElement>) {
    // On radio selection change. Update Item object to selected : true
    // Change previously selected Item to false.
    //
    // Item {
    //   ...
    //   selected: true
    // }

    const itemName = evt.target.id;
    const groupKey = evt.target.name;

    const partitionedItemsCopy = { ...partitionedItems }
    const itemsArr = partitionedItemsCopy[groupKey];
    
    const itemsArrCopy = itemsArr.map(({ ...curItem }) => {
      if (curItem.name === itemName) {
        curItem.selected = true;
      } else {
        curItem.selected = false;
      }

      return curItem;
    }) as Item[];

    partitionedItemsCopy[groupKey] = itemsArrCopy;
    setPartitionedItems(partitionedItemsCopy);
  }

  function handleNext() {
    setActiveStep((prevActiveStep) => prevActiveStep + 1);
  };

  function handleBack() {
    setActiveStep((prevActiveStep) => prevActiveStep - 1);
  };

  function handleReset() {
    setActiveStep(0);
  };

  function formatedPriceRange(choiceObj: Item): string {
    const lowPrice = formatNumber(String(choiceObj.lowPrice));
    const highPrice = formatNumber(String(choiceObj.highPrice));

    return choiceObj.name !== 'none' ? `${lowPrice}-${highPrice}` : '';
  }

  function stepperJSX() {
    return (
      <Box sx={{ minWidth: 300, marginLeft: 10, marginBottom: 10 }}>
        <Box sx={{ mx: 'auto' }}>
          <Stepper activeStep={activeStep} orientation="vertical">
            {stepNames.map((stepName, index) => (
              <Step key={index}>
                <StepLabel
                  optional={
                    index === 2 ? (
                      <Typography variant="caption">Last step</Typography>
                    ) : null
                  }
                >
                  {stepName}
                </StepLabel>
                <StepContent>
                  <FormControl>
                    {partitionedItems[stepName].map((choiceObj, idx) => (
                      <Box sx={{ margin: 0.5}}  key={idx}>
                        <label>
                          <input 
                            onChange={handleChange}
                            type="radio"
                            id={choiceObj.name}
                            name={stepName} 
                            checked={choiceObj.selected === true}
                          />
                          {`${choiceObj.name}`}
                          <Box 
                            component="span" 
                            sx={{fontSize: 12, paddingLeft: 1}}
                          >
                            {`${formatedPriceRange(choiceObj)}`}
                          </Box>
                        </label>
                      </Box>
                    ))}
                    <Box sx={{ mb: 2 }}>
                      <Button
                        variant="contained"
                        onClick={handleNext}
                        sx={{ mt: 1, mr: 1 }}
                      >
                        {index === stepNames.length - 1 ? 'Finish' : 'Continue'}
                      </Button>
                      {index !== 0 &&
                        <Button
                          hidden={index === 0}
                          onClick={handleBack}
                          sx={{ mt: 1, mr: 1 }}
                        >
                          Back
                        </Button>
                      }
                    </Box>
                  </FormControl>
                </StepContent>
              </Step>
            ))}
          </Stepper>
          {activeStep === stepNames.length && (
            <Paper square elevation={0} sx={{ p: 3 }}>
              <Typography>All steps completed - you&apos;re finished</Typography>
              <Button onClick={handleReset} sx={{ mt: 1, mr: 1 }}>
                Reset
              </Button>
            </Paper>
          )}
        </Box>
      </Box>
    )
  }

  return (
    <Fragment>
      {
        stepNames.length === 0 
        ? 
          <h4>Loading...</h4> 
        :
          stepperJSX()
      }
    </Fragment>
  )
}

export default BudgetStepper;
