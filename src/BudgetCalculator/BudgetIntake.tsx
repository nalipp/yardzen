import { useState, SetStateAction } from 'react';

import { 
  Box,
  Button,
  TextField,
  Card,
  CardContent 
} from '@material-ui/core';

interface Props {
  setBudget: React.Dispatch<SetStateAction<any>>;
  setBudgetSubmited: React.Dispatch<SetStateAction<boolean>>;
  budget: number;
}

function BudgetIntake({ 
    budget,
    setBudget,
    setBudgetSubmited 
  }: Props) {

  //
  // BudgetIntake is a form allowing user to submit their budget
  //   Currently an error only occurs when the user 
  //   submits 0 or a negative number
  //

  const [ budgetError, setBudgetError ] = useState<string>('');

  function handleChange(evt: React.ChangeEvent<HTMLInputElement>) {
    setBudget(Number(evt.target.value)); 
  }

  function handleSubmit(evt: React.FormEvent<HTMLInputElement>) {
    evt.preventDefault();
    setBudgetError('');

    if (budget <= 0) {
      setBudgetError('Budget must be larger than 0');
    } else {
      setBudgetSubmited(true);
    }
  }

  return(
    <Box sx={{ margin: 'auto', marginTop: 160, width: 295}}>
      <Card>
        <CardContent>
        
          <Box sx={{ fontSize: 20, margin: 25 }}>
            Please enter your project budget range
          </Box>
          
          <Box sx={{ fontSize: 10, margin: 25, color: 'tomato' }}>
            {budgetError}
          </Box>
          <Box
            component="form"
            onSubmit={handleSubmit}
            sx={{margin: 25}}
          >
            <Box sx={{ marginBottom: 8 }}>
              <TextField 
                onChange={handleChange}
                id="outlined-basic"
                name="budget"
                label="Budget"
                variant="outlined"
                type="number"
                value={budget}
              />
            </Box>

            <Button 
              variant="contained"
              type="submit"
            >
              Submit
            </Button>
          </Box>
        
        </CardContent>
      </Card>
    </Box>
  )
}

export default BudgetIntake;
