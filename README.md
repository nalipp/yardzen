
Typescript was slow going at the beginning but I feel like I am getting the hang of it.

This is also my first exposure to Material UI but found the styling nicer than Bootstrap.

I will plan to continue working with this app to learn testing with Typescript but wanted to submit first.


#### Details
    src/
        BudgetCalculator/
            BudgetIntake.tsx	
            BudgetStepper.tsx	
            EstimateTracker.tsx	
            Main.tsx
            
        helpers.tsx
        api.tsx

When Main.tsx is loaded a request is made to fetch an array of Item objects from the api.

These objects grouped by their type in a partitionedObject. 

```
{
  typeName1: [{Item object}, {...}, {...}],
  typeName2: [{Item object}, {...}],
  ...
}
```

Users will input their budget into BudgetIntake.txt 

On Submit the BudgetStepper.tsx component will display, prompting the user through a series 
of radio inputs for selecting a single Item object belonging to a particular type.

The state of the users selections will be held in the partitionedObject, 
when a user selects an Item object a key of selected will be set to true.

The partitionedObject will be the source of truth for calculating the min and max of the 
current estimate.

At any time the user can readjust their budget withouth affecting their current 
position in the BudgetStepper component. (This is why state for the current step lives in Main) 


### Todo

React testing with Typescript


