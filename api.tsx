import { initializeApp } from 'firebase/app';
import { getFirestore, collection, getDocs } from 'firebase/firestore/lite';

/**
*  The firebase credentials should be placed in an .env file 
*  but skipped this step so the app can be cloned more easily
*
**/

const firebaseConfig = {
  apiKey: "AIzaSyD7NUVfrImccSo8FuCBG7bXVk0oLFqgE-k",
  authDomain: "yardzen-demo.firebaseapp.com",
  databaseURL: "https://yardzen-demo.firebaseio.com",
  projectId: "yardzen-demo",
  storageBucket: "yardzen-demo.appspot.com",
  messagingSenderId: "509183652730",
  appId: "1:509183652730:web:ba2208f7d8e0882f009cc3"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

/* getIetms()
   Firebase request for items column
   result =>
     [
       {
         type: 'WATER_FEATURES', 
         lowPrice: 2000000, 
         name: 'Fountain  
         highPrice: 6000000
       }, 
       {...},
       {...},
     ]
*/

interface Item {
  type: string;
  name: string;
  lowPrice: number;
  highPrice: number;
}

async function getItems(): Promise<any[]> {
  const itemsCol = collection(db, 'items');
  const itemSnapshot = await getDocs(itemsCol);
  const itemList = itemSnapshot.docs.map(doc => doc.data());
  
  return itemList;
}


export default getItems;
